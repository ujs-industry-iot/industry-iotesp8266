// 引入 wifi 模块，并实例化，不同的芯片这里的依赖可能不同
#include <ESP8266WiFi.h>
#include <SimpleDHT.h>
static WiFiClient espClient;
// 引入阿里云 IoT SDK
#include <AliyunIoTSDK.h>

// 设置产品和设备的信息，从阿里云设备信息里查看
#define PRODUCT_KEY "a1CYi0PLB9K"
#define DEVICE_NAME "test"
#define DEVICE_SECRET "3a0aaf28258ae88f22cfa36f4848ccea"
#define REGION_ID "cn-shanghai"

// 设置 wifi 信息
#define WIFI_SSID "Wlan_STRY"
#define WIFI_PASSWD "13056175796"

//设置温湿度传感器为D2
int pinDHT11 = D2; 
int switch_status=0;
SimpleDHT11 dht11(pinDHT11);
void setup()
{
    //初始化关闭灯
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.begin(115200);
    //将内置的LED灯接口设置为输出 GPIO2
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(D1, INPUT);
    // 初始化 wifi
    wifiInit(WIFI_SSID, WIFI_PASSWD);

    // 初始化 iot，需传入 wifi 的 client，和设备产品信息
    AliyunIoTSDK::begin(espClient, PRODUCT_KEY, DEVICE_NAME, DEVICE_SECRET, REGION_ID);

    // 绑定一个设备属性回调，当远程修改此属性，会触发 powerCallback
    // LEDSwitch 是在设备产品中定义的物联网模型的 id
    AliyunIoTSDK::bindData("Switch:LEDSwitch", powerCallback);

}

void loop()
{
    int illumination = analogRead(A0); //光感模拟口接esp8266AO口
    illumination = 1023 - illumination;
    illumination = illumination / 10.23 - 1; //将光照强度设置为0-99
    //mqtt消息内容
    char parms[256];
    byte temperature = 0;
    byte humidity = 0;
    int err = SimpleDHTErrSuccess;
    if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess)
    {
        Serial.print("Read DHT11 failed, err=");
        Serial.println(err);
        return;
    }
    sprintf(parms, "{\"temperature\":%d,\"humidity\":%d,\"illumination\":%d,\"Switch:LEDSwitch\":%d}", (int)temperature, (int)humidity,illumination,switch_status);
    Serial.print(parms);
    delay(1000);
    AliyunIoTSDK::loop();
    AliyunIoTSDK::send(parms);
}

// 初始化 wifi 连接
void wifiInit(const char *ssid, const char *passphrase)
{
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, passphrase);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(1000);
        Serial.println("WiFi not Connect");
    }
    Serial.println("Connected to AP");
}

// 电源属性修改的回调函数
void powerCallback(JsonVariant p)
{
    int PowerSwitch = p["Switch:LEDSwitch"];
    Serial.println("led");
    switch_status=PowerSwitch;
    if (PowerSwitch == 1)
    {
        Serial.println("on");
        digitalWrite(LED_BUILTIN, LOW); // Turn the LED on (Note that LOW is the voltage level
    }
    else
    {
        Serial.println("off");
        digitalWrite(LED_BUILTIN, HIGH);
    }
}
